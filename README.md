# Base Converter

Convert almost any number between any bases(>=2).

Doesn't implement big numbers, they won't overflow but won't convert either.

Interface is in portuguese.

## Build

`gcc main.cpp -lm -o converter`

Tested on Fedora 29 using gcc 8.2.1 and clang 7.0

## Execute

`./converter`
