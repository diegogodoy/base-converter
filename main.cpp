// Created by diego on 4/13/16.
//Compile with g++ -std=c++11 base_conversion.cpp

#include <iostream>
#include <cmath>
#include <exception>

using namespace std;
using ull = unsigned long long int;

int stoi(char ch){
	string temp;
	temp = ch;
	return stoi(temp);

}

bool isValid(const string& number){
	bool point = false;
	for(auto& i : number){
		if(!isdigit((int) i )){
			if((i == '.') && !point) point = true;
			else return false;
		}
	}
	return true;
}

bool isBaseValid(const string& number, size_t base){
	for(auto& i : number) if(isdigit((int) i) && (size_t)stoi(i) >= base) return false;
	return true;
}

bool hasPoint(string number){
	for(auto& i : number) if( i == '.') return true;
	return false;
}

void splitByPoint(const string& input, string& left, string& right){
	if(!isValid(input)) throw runtime_error("Entrada inválida:(não é númerico ou é muito longo)");
	bool point(false);
	for(auto& i : input){
		if(i == '.') {
			point = true;
			continue;
		}
		if(!point) left += i;
		else right += i;

	}
}

double toBase10(const string& number, size_t base) {
	if(!isValid(number)) throw runtime_error("Entrada inválida:(não é númerico ou é muito longo)");
	if(!isBaseValid(number, base)) throw runtime_error("Número informado não está na base informada");
	double p(0);
	string left, right;
	splitByPoint(number, left, right);
	{
		size_t expoent(left.size()-1);
		for(auto& i : left) p += stoi(i) * pow(base, expoent--);
	}{
		size_t expoent(1);
		for(auto& i : right) p += stoi(i) / pow(base, expoent++);
	}
	return p;
}

char digitToChar(size_t in){
	switch (in){
		case 0 : return '0';
		case 1 : return '1';
		case 2 : return '2';
		case 3 : return '3';
		case 4 : return '4';
		case 5 : return '5';
		case 6 : return '6';
		case 7 : return '7';
		case 8 : return '8';
		case 9 : return '9';
		default: throw runtime_error("entrada contem caractere inválido");
	}
}

void invert(string& toInvert){
	string temp;
	for(auto i = toInvert.rbegin(); i != toInvert.rend(); ++i) temp.push_back(*i);
	toInvert = temp;
}

string base10To(string number, size_t base){
	string left10, right10, oLeft, oRight;
	splitByPoint(number, left10, right10);
	{
		auto integer  = stoll(left10);
		while(integer > 0){
			auto resto =  integer % base;
			oLeft += digitToChar(resto);
			integer /= base;
		}
		invert(oLeft);
	}{
		double r(stod("0." + right10));
		while(oRight.size() <= 20){
			auto item = r*base;
			string digl, digr;
			splitByPoint(to_string(item), digl, digr);
			oRight += digl;
			r = stod("0."+ digr);
		}
	}
	return oLeft + '.' + oRight;
}

int main(){
	//cout << "O retorno será dado com 20 casas decimais" << endl;
	cout << "Para sair, insira uma base negativa" << endl;
	for(;;){
		cout << "Digite em linhas separadas a base de entrada, seguido da base de saida, e então numero. " << endl;
		string numero, baseinS, baseoutS;
		getline(cin, baseinS);
		if(stoi(baseinS) < 0){
			cout << "Deseja sair? (y/n)";
			string temp;
			getline(cin, temp);
			if(temp == "y") return 0;
			else continue;
		}
		getline(cin, baseoutS);
		getline(cin, numero);
		size_t basein(0), baseout(0);

		try {
			basein = stoul(baseinS);
			baseout = stoul(baseoutS);
		} catch (exception& e){
			cout << e.what() << endl;
			continue;
		}
		//cout.precision(10);

		if((basein == 0) || (baseout == 0) || (basein == 1) || ( baseout == 1)){
			cout << "Base não pode ser 0, ou 1" << endl;
			continue;
		}
		try{
			if(basein == baseout)
				cout << numero << endl;
			if(basein == 10)
				cout << base10To(numero, baseout) << endl;
			else
				if(baseout == 10)
					cout << toBase10(numero, basein) << endl;
				else
					cout << base10To(to_string(toBase10(numero, basein)), baseout) << endl;
		} catch (exception& e){
			cout << e.what() /*<< " linha: " << __LINE__*/ << endl;
			cout << "Não foi possível converter entre as bases" << endl;
			continue;
		}
	}
}
